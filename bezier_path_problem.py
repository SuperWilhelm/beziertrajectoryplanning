
class BezierPathProblem:
    def __init__(self, convex_free_space, bezier_degree,
                 start_point, end_point):
        '''
            Args:
                convex_free_space: ConvrexFreeSpace object describing the
                    valid space for searching
                bezier_degree: The degree of the sub bezier curves
                start_point: The start_point of the search
                end_point: The end_point of the search
                max_velocity: The maximum velocity constraint for all
                    points in time of the solution curve
                max_acc The maximum acceleration constraint for all
                    points in time of the solution curve
        '''
        self.convex_free_space = convex_free_space
        self.dim = convex_free_space.dim
        self.bezier_degree = bezier_degree
        self.start_point = start_point
        self.end_point = end_point
        self.max_vel = 3
        self.max_acc = 3
        self.average_vel_heuristic = 2
