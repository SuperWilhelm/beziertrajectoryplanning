from matplotlib import pyplot as plt
import numpy as np
import mpl_toolkits.mplot3d.axes3d as p3
from matplotlib import animation
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, \
                                       Line3DCollection


class ThreeDimensionalPlotter:
    def __init__(self, polytopes, start_point, end_point,
                 solution_bezier_curve):
        '''
            Args:
                polytopes: The polytopes which are to be plotted
                start_point: The start point of the problem
                end_point: The end point of the problem
                solution_bezier_curve: BezierCurveSequence describing
                    the solution curve
        '''
        self.polytopes = polytopes
        self.start_point = start_point
        self.end_point = end_point
        self.solution_bezier_curve = solution_bezier_curve

    def plot(self):
        fig = plt.figure()
        ax = p3.Axes3D(fig)

        def plot_polytope(polytope):
            '''
                This method will plot a single polytope
            '''
            # vertices of a pyramid
            print("Trying to plot polytope")
            v = np.array([vert.coords for vert in polytope.vertices])

            ax.scatter3D(v[:, 0], v[:, 1], v[:, 2], s=2)

            x_s = list(set(list(v[:, 0])))
            y_s = list(set(list(v[:, 1])))
            z_s = list(set(list(v[:, 2])))

            verts = [
                v[v[:, 0] == x_s[0]].tolist(),
                v[v[:, 0] == x_s[1]].tolist(),
                v[v[:, 1] == y_s[0]].tolist(),
                v[v[:, 1] == y_s[1]].tolist(),
                v[v[:, 2] == z_s[0]].tolist(),
                v[v[:, 2] == z_s[1]].tolist()
                ]

            verts = [[x[0], x[2], x[3], x[1]] for x in verts]

            # # generate list of sides' polygons of our pyramid
            # verts = [[v[0], v[1], v[4]], [v[0], v[3], v[4]],
            #          [v[2], v[1], v[4]], [v[2], v[3], v[4]],
            #          [v[0], v[1], v[2], v[3]]]

            # plot sides
            ax.add_collection3d(Poly3DCollection(
                verts, facecolors='cyan', linewidths=1, edgecolors='red',
                alpha=.15))

        def gen(n):
            phi = 0
            while phi < 2*np.pi:
                yield np.array([np.cos(phi), np.sin(phi), phi])
                phi += 2*np.pi/n

        def update(num, data, line):
            line.set_data(data[:2, :num])
            line.set_3d_properties(data[2, :num])

        N = int(5*self.solution_bezier_curve.duration)
        data = np.array(list(
            [np.array(self.solution_bezier_curve.evaluate(i/int(5*self.solution_bezier_curve.duration)).coords)
             for i in range(int(5*self.solution_bezier_curve.duration))]
            )).T
        line, = ax.plot(data[0, 0:1], data[1, 0:1], data[2, 0:1])

        ax.scatter3D(self.start_point.coords[0],
                     self.start_point.coords[1],
                     self.start_point.coords[2],
                     color='r', s=25)
        ax.scatter3D(self.end_point.coords[0],
                     self.end_point.coords[1],
                     self.end_point.coords[2],
                     color='r', s=25)
        ani = animation.FuncAnimation(fig, update, N, fargs=(data, line), interval=100/N, blit=False)
        #ani.save('matplot003.gif', writer='imagemagick')
        [plot_polytope(polytope) for polytope in self.polytopes]
        plt.show()