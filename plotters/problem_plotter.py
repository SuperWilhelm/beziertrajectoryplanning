import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection


class BezierCurveSequencePlotter:
    def plot(convex_free_space, solution_polytopes, solution_bezier_sequence,
             start_point, end_point):
        '''
            Args:
                convrx_free_space: ConvrxFreeSpace associated with the problem
                solution_polytopes: The sequence of polytopes from the current
                    point to the solution
                solution_beizer_sequence: The solution curve to the problem,
                    described in the form [BezierCurve]
                start_point: The start point of the problem
                end_point: The end point of the problem
        '''
        def next_frame(i, fig, trace_plot):
            time = (float(i) * 0.04)

            point = solution_bezier_sequence.evaluate(time).coords
            trace_plot.set_offsets((point[0], point[1]))
            return trace_plot,

        duration = sum([x.duration
                        for x in solution_bezier_sequence.bezier_curves])

        used_free_space = []
        for polytope in convex_free_space.polytopes:
            polygon = Polygon([x.coords for x in polytope.vertices], True)
            used_free_space.append(polygon)

        p = PatchCollection(used_free_space, alpha=0.4)

        fig = plt.figure()

        x = [0]
        y = [0]

        ax = fig.add_subplot(111)
        ax.add_collection(p)
        ax.set_xlim([0, 30])
        ax.set_ylim([0, 10])

        trace_plot = plt.scatter(x, y)
        plt.scatter([start_point.coords[0]], [start_point.coords[1]],
                    color='red')
        plt.scatter([end_point.coords[0]], [end_point.coords[1]],
                    color='green')
        trace_plot.set_alpha(0.8)

        anim = animation.FuncAnimation(fig, next_frame,
                                       fargs=(fig, trace_plot),
                                       frames=int(25*duration), interval=25)

        plt.show()
