import numpy as np
from geometry.convex_free_space import ConvexFreeSpace
from geometry.point import Point
from geometry.linear_constraint import LinearConstraint
from geometry.polytope import Polytope
from plotters.three_dimensional_plotter import ThreeDimensionalPlotter


class Random3DProblemGenerator:
    '''
        This class will provide functionality to generate
        random ConvexFreeSpace objects in 3D, in the
        positive region of cartesian space. Will also produce
        a start point and an end point which is feasable
    '''
    def __init__(self, no_polytopes, start_point=Point([90, 90, 90]),
                 max_len=10):
        '''
            Args:
                no_polytopes: The number of polytopes to plot
                start_point: The starting point of the search
                max_len: The maximum side length of each of the polytopes
        '''
        self.no_polytopes = no_polytopes
        self.start_point = start_point
        self.max_len = max_len

    def generate_random(self):
        '''
            Returns:
                ConvrxFreeSpace: Which contains at least one path
                    from the start point to the end point.
        '''
        def generate_new_polytope(center):
            '''
                Args:
                    center: The center point of the generation
            '''
            x_up = center.coords[0] + np.random.uniform(1, self.max_len)
            x_down = center.coords[0] - np.random.uniform(1, self.max_len)
            y_up = center.coords[1] + np.random.uniform(1, self.max_len)
            y_down = center.coords[1] - np.random.uniform(1, self.max_len)
            z_up = center.coords[2] + np.random.uniform(1, self.max_len)
            z_down = center.coords[2] - np.random.uniform(1, self.max_len)

            x_up_plane = LinearConstraint([1, 0, 0], x_up, center)
            x_down_plane = LinearConstraint([1, 0, 0], x_down, center)
            y_up_plane = LinearConstraint([0, 1, 0], y_up, center)
            y_down_plane = LinearConstraint([0, 1, 0], y_down, center)
            z_up_plane = LinearConstraint([0, 0, 1], z_up, center)
            z_down_plane = LinearConstraint([0, 0, 1], z_down, center)

            polytope_planes = [x_up_plane, x_down_plane, y_up_plane,
                               y_down_plane, z_up_plane, z_down_plane]
            return Polytope(polytope_planes)

        polytopes = []
        current_point = self.start_point

        for _ in range(self.no_polytopes):
            new_polytope = generate_new_polytope(current_point)
            polytopes.append(new_polytope)
            current_point = new_polytope.random_point()

        goal_point = polytopes[-1].random_point()
        return ConvexFreeSpace(polytopes), goal_point


random_generator = Random3DProblemGenerator(5)

convex_free_space, goal_point = random_generator.generate_random()

plotter = ThreeDimensionalPlotter(convex_free_space.polytopes,
                                  Point([90, 90, 90]),
                                  goal_point, None)
plotter.plot()
