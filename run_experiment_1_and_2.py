import pickle
# Import the solvers
from solvers.bilevel_optimization_solver import BilevelOptimization
from solvers.non_convex_trust_region import NonConvexTrustRegion
from solvers.quadratic_program_multi_polytope_solver import \
    QuadraticProgramMultiPolytopeSolver
from solvers.fixed_time_section_independent_quadratic_programming import \
    HeuristicTimeQuadraticProgrammingSectionIndependent
from solvers.deCasteljau_optimization import DeCasteljauOptimization
from plotters.test_plotter import ThreeDimensionalPlotter

# Load the previously generated random problems from file
with open('1000_rand_3D_probs.pkl', 'rb') as f:
    random_problems = pickle.load(f)

# store the solvers in a dictionary
solvers = {
    #'naive_fixed_time_qp': HeuristicTimeQuadraticProgrammingSectionIndependent,
    'sofisticated_fixed_time_qp': QuadraticProgramMultiPolytopeSolver,
    #'de_castle_alg_fixed_time:': DeCasteljauOptimization,
    #'trust_region_time_variable': NonConvexTrustRegion,
    #'bi_level_optimization': BilevelOptimization
}

# Create a dict to store the results of the experiments
# Each list will store (time, cost) pairs for the ith random_problems
results = {
    #'naive_fixed_time_qp': [],
    'sofisticated_fixed_time_qp': [],
    #'de_castle_alg_fixed_time:': [],
    #'trust_region_time_variable': [],
    #'bi_level_optimization': []
}

# Get each of the solvers to solve each of the problems
# This is the problem which we want to attack.
# Question: Why is this particular problem overconstrained?

for problem in random_problems:
    problem.bezier_degree = 7
    for solver_name in solvers:
        solver = solvers[solver_name]
        solver_instance = solver(problem)
        bezier_sequence, polytopes, _ = solver_instance.solve()
        plotter = ThreeDimensionalPlotter(polytopes,
                                          problem.start_point,
                                          problem.end_point,
                                          bezier_sequence)

        plotter.plot()
