from plotters.three_dimensional_plotter import ThreeDimensionalPlotter
import pickle

# Load the previously generated random problems from file
with open('1000_rand_3D_probs.pkl', 'rb') as f:
    random_problems = pickle.load(f)


for test_problem in random_problems:
    shortest_path = \
        test_problem.convex_free_space.get_shortest_path(
            test_problem.start_point, test_problem.end_point)

    polytopes = [x[0] for x in shortest_path][:-1]
        # self.n_curves = len(self.polytopes)
        # self.adjacent_polytopes = list(zip(self.polytopes,
        #                                    self.polytopes[1:]))
    plotter = ThreeDimensionalPlotter(polytopes,
                                      test_problem.start_point,
                                      test_problem.end_point,
                                      None)

    plotter.plot()
