from geometry.linear_constraint import LinearConstraint
from geometry.polytope import Polytope
from geometry.convex_free_space import ConvexFreeSpace
from geometry.point import Point
from geometry.bezier_curve import BezierCurve
from geometry.bezier_curve_sequence import BezierCurveSequence
from plotters.problem_plotter import BezierCurveSequencePlotter
from bezier_path_problem import BezierPathProblem
# All of the solvers look like this:
from solvers.fixed_time_section_independent_quadratic_programming import \
    HeuristicTimeQuadraticProgrammingSectionIndependent
from solvers.quadratic_program_multi_polytope_solver import \
    QuadraticProgramMultiPolytopeSolver
from solvers.deCasteljau_optimization import DeCasteljauOptimization
from solvers.bilevel_optimization_solver import BilevelOptimization
from solvers.non_convex_trust_region import NonConvexTrustRegion

import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--solver", dest="solver",
                    default="naive_solver", help="Solver name")

# Define some polytioes
# This will be a 2d problem
# Polytope 1: (0,0), (0,4), (4,4), (4,0)
edge_1 = LinearConstraint([0, 1], 4, Point((1, 1)))
edge_2 = LinearConstraint([0, 1], 0, Point((1, 1)))
edge_3 = LinearConstraint([1, 0], 4, Point((1, 1)))
edge_4 = LinearConstraint([1, 0], 0, Point((1, 1)))

polytope_1 = Polytope([edge_1, edge_2, edge_3, edge_4])

# Polytope 3: (2,2), (2,6), (12, 6), (12,2)
edge_1 = LinearConstraint([0, 1], 2, Point((4, 4)))
edge_2 = LinearConstraint([0, 1], 6, Point((4, 4)))
edge_3 = LinearConstraint([1, 0], 2, Point((4, 4)))
edge_4 = LinearConstraint([1, 0], 16, Point((4, 4)))

polytope_2 = Polytope([edge_1, edge_2, edge_3, edge_4])


# Polytope: (15,4), (15,0), (22, 0), (22,4)
edge_1 = LinearConstraint([0, 1], 4, Point((16, 2)))
edge_2 = LinearConstraint([0, 1], 0, Point((16, 2)))
edge_3 = LinearConstraint([1, 0], 15, Point((16, 2)))
edge_4 = LinearConstraint([1, 0], 22, Point((16, 2)))

polytope_3 = Polytope([edge_1, edge_2, edge_3, edge_4])

# Set up an example problem
convex_free_space = ConvexFreeSpace([polytope_1, polytope_2, polytope_3])
start_point = Point((1, 1))
end_point = Point((21, 3))

b_1 = BezierCurve([Point((0, 0)), Point((3, 3))], 2)
b_2 = BezierCurve([Point((3, 3)), Point((17, 3))], 2)
bezier_curve_sequence = BezierCurveSequence([b_1, b_2])


bezier_degree = 8
bezier_path_problem = BezierPathProblem(convex_free_space, bezier_degree,
                                        start_point, end_point)

args = parser.parse_args()
if args.solver == "naive_solver":
    solver = HeuristicTimeQuadraticProgrammingSectionIndependent(bezier_path_problem)
elif args.solver == "sophisticated_solver":
    solver = QuadraticProgramMultiPolytopeSolver(bezier_path_problem)
elif args.solver == "de_casteljau":
    solver = DeCasteljauOptimization(bezier_path_problem)
elif args.solver == "non_convex_solver":
    solver = NonConvexTrustRegion(bezier_path_problem)
elif args.solver == "bilevel_opt":
    solver = BilevelOptimization(bezier_path_problem)

solution_polytopes, bezier_curve_sequence, cost, solve_time = solver.solve()

start_point = bezier_path_problem.start_point
end_point = bezier_path_problem.end_point

plotter = BezierCurveSequencePlotter.plot(convex_free_space,
                                          solution_polytopes,
                                          bezier_curve_sequence,
                                          start_point, end_point)
