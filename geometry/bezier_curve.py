from geometry.bernstein_polynomial import bernstein_poly


class BezierCurve:
    def __init__(self, control_points, duration=1):
        self.control_points = control_points
        self.duration = duration
        self.degree = len(self.control_points) - 1

    def evaluate(self, time):
        '''
            Args:
                time: The point in time to evaluate the Bezier curve
        '''
        return sum([bernstein_poly(i, self.degree, time, self.duration)
                    * self.control_points[i]
                    for i in range(self.degree+1)])
