import numpy as np


class Point:
    def __init__(self, coords):
        '''
            coords: The coordinates defining the points as a list
        '''
        coords = np.array(coords)
        self.coords = coords
        self.dim = len(coords)

    def __str__(self):
        return "{}".format(self.coords)

    def __mul__(self, scalar):
        return Point(scalar*self.coords)

    def __rmul__(self, scalar):
        return Point(scalar*self.coords)

    def __radd__(self, other):
        if other == 0:
            return self
        else:
            return self.__add__(other)

    def __add__(self, b):
        return Point(self.coords + b.coords)

    @staticmethod
    def mean(points):
        '''
            Args:
                points: List of points
            Return:
                Point which is the mean of the points
        '''
        if list(points) == []:
            return None
        else:
            return Point(np.mean([point.coords for point in points], axis=0))

    @staticmethod
    def distance(point_1, point_2):
        '''
            Args:
                point_1: The first point
                point_2: The second point
            Returns:
                d_2 distance between the two points
        '''
        return np.linalg.norm(np.array(point_1.coords) -
                              np.array(point_2.coords))

    @staticmethod
    def path_distance(points):
        '''
            Args:
                points: List of points
            Returns:
                The distance (d_2 norm) when traversing points
                in order
        '''
        distance = 0
        for i in range(len(points) - 1):
            point_1 = points[i]
            point_2 = points[i+1]
            distance += Point.distance(point_1,
                                       point_2)
        return distance
