from geometry.polytope import Polytope
from geometry.point import Point
import heapq
import itertools


class ConvexFreeSpace:
    def __init__(self, polytopes):
        '''
            polytopes: dict of the polytopes which make up the
                ConvexFreeSpace
        '''
        self.polytopes = polytopes
        self.dim = polytopes[0].dim
        self.find_intersections()

    def find_intersections(self):
        '''
            This method will create an instance variable
            intersection_graph wgich will describe the intere
        '''
        def get_intersection(polytope_1, polytope_2):
            '''
                Args:
                    polytope_1: First Polytope object
                    polytope_2: Second Polytope object
                Returns:
                    Polytope object describing the intersection of
                    the two polytopes
            '''
            return Polytope(polytope_1.planes + polytope_2.planes)
        # Get pairs of polytopes
        pairs = list(itertools.combinations(self.polytopes, 2))
        graph = {}
        for pair in pairs:
            polytope_1 = pair[0]
            polytope_2 = pair[1]
            intersection_polytope = get_intersection(polytope_1, polytope_2)
            if intersection_polytope.is_non_empty():
                # Add the intersection to the intersection graph
                if polytope_1 not in graph:
                    graph[polytope_1] = {}

                graph[polytope_1][polytope_2] = intersection_polytope

                if polytope_2 not in graph:
                    graph[polytope_2] = {}

                graph[polytope_2][polytope_1] = intersection_polytope

        self.intersection_graph = graph

    def get_shortest_path(self, start, end):
        '''
            Args:
                start: A Point describing the start point of
                    the path
                end: A Point describing the end point of the
                    path
            Returns:
                List of polytope keys which have the shortest d_2
                norm between the polytope center of mass. Also included
                is will be a list of intersecions of the polytopes
                The form will be:
                    (list of n polytopes ordered from start to end,
                     list of n-1 polytopes describing the intersection of
                      consecutive polytopes)
        '''
        d = Point.path_distance
        # This algorithm will use the A* Search Alg.
        frontier = []
        # Add variables to contain the best solution so far
        shortest_path = None
        shortest_path_cost = None

        # Initialize the search frontier with the current polytopes
        for polytope in self.polytopes:
            if polytope.contains_point(start):
                try:
                    heapq.heappush(frontier,
                                   (d([start, end]),            # cost
                                    [(polytope, start, None)])) # path
                except:
                    pass

        # Initialize what the goal polytopes are
        goal_polytopes = [x for x in self.polytopes if
                          x.contains_point(end)]

        while frontier != []:
            current_cost, current_path = heapq.heappop(
                frontier)
            # If the current cost is greater than the shortest_path_cost
            # then optimal has been found so break
            if shortest_path is not None and \
                    shortest_path_cost <= current_cost:
                break

            current_polytope, current_point, _ = current_path[-1]

            # Goal Test
            if current_polytope in goal_polytopes:
                if shortest_path_cost is None or \
                        current_cost < shortest_path_cost:
                    shortest_path = current_path + [(current_polytope, end, None)]
                    shortest_path_cost = current_cost

                continue

            # Goal test has failed. Generate future paths from this point
            visited_polytopes = [x[0] for x in current_path]
            visited_points = [x[1] for x in current_path]
            intersection_polytope = [x[2] for x in current_path]
            candidate_next_polytopes = \
                self.intersection_graph[current_polytope].keys()
            valid_next_polytopes = [x for x in candidate_next_polytopes
                                    if x not in visited_polytopes]

            for next_polytope in valid_next_polytopes:
                if next_polytope in goal_polytopes:
                    intersection_point = end
                else:
                    intersection_point = \
                        next_polytope.mean
                new_visited_polytopes = visited_polytopes + [next_polytope]
                new_visited_points = visited_points + [intersection_point]
                intersection_polytope = intersection_polytope + \
                    [self.intersection_graph[current_polytope][next_polytope]]
                new_path = list(zip(new_visited_polytopes,
                                    new_visited_points,
                                    intersection_polytope))

                new_path_cost = d(new_visited_points+[end])
                heapq.heappush(frontier, (new_path_cost, new_path))
        # Only return the polytopes for the optimal path
        return shortest_path
