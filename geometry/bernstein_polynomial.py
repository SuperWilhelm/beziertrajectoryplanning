from sympy import binomial


def bernstein_poly(i, n, time, time_max=1):
    '''
        Args:
            i: index
            n: degree
            time: symbolic time vatiable
            time_max: Float. The time duration of the curve
        Returns:
            sympy formula describing
    '''
    return binomial(n, i) * (time/time_max)**i * \
        (1 - (time/time_max))**(n-i)
