from sympy import symbols, diff, integrate, expand, lambdify
from sympy.core.symbol import Symbol
from geometry.bernstein_polynomial import bernstein_poly
from geometry.point import Point
import numpy as np


class BezierCurveSequence:
    def __init__(self, bezier_curves):
        '''
            Args:
                bezier_curves: [BezierCurves]
        '''
        self.bezier_curves = bezier_curves
        self.duration = sum([x.duration for x in self.bezier_curves])

    def evaluate(self, time):
        '''
            Arguments:
                time: The time at which the bezier curve is to be
        '''
        # find the start times of each of the polynomials
        initial_times = np.cumsum([x.duration for x in self.bezier_curves])

        curve_index = None
        for i in range(len(initial_times)):
            if time < initial_times[i]:
                curve_index = i
                break
        duration_until_start = [0] + list(initial_times)
        if curve_index is None:
            raise ValueError("At the time {} the sequence is complete".format(
                time))
        return self.bezier_curves[curve_index].evaluate(
            time-duration_until_start[curve_index])

    @staticmethod
    def get_cost_for_curve(dimensions, degree, curve_index, start_point=None,
                           end_point=None, zero_vel_start=False,
                           zero_vel_end=False, var_post_fix=''):
        '''
            Args:
                dimensions: The number dimensions which the curve is in
                degree: The degree of the curve
                start_point: The point which the curve starts at in dict format
                end_point: The point which the curve ends at in dict format
                zero_vel_start: True iff the velocity at the start of the curve
                    is forced to be zero
                zero_vel_end: True iff the velocity at the end of the curve is
                    forced to be zero
                var_post_fix: A string to append to the end of variable names
            Returns:
                f, symbols_dict = {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}}
        '''
        def get_curve_in_dim(dimension_index, var_dict, time_variable):
            return sum([bernstein_poly(cp_index,
                                       degree,
                                       time_variable,
                                       var_dict['t']) *
                        var_dict['p'][cp_index][dimension_index]
                        for cp_index in range(degree + 1)])

        # Create a dict of the variables for this curve
        var_dict = {'t': symbols('t_{}'.format(curve_index))}
        state_vars = {}
        for cp_index in range(degree+1):
            if cp_index == 0 and start_point is not None:
                state_vars[cp_index] = start_point
            elif (cp_index == 1 or cp_index == 2) and zero_vel_start:
                state_vars[cp_index] = state_vars[0]
            elif cp_index == degree - 2 and end_point is not None and \
                    zero_vel_end:
                state_vars[cp_index] = end_point
            elif cp_index == degree - 1 and end_point is not None and \
                    zero_vel_end:
                state_vars[cp_index] = end_point
            elif cp_index == degree and zero_vel_end:
                state_vars[cp_index] = state_vars[cp_index - 1]
            else:
                # In this case new variables need to be established
                state_vars[cp_index] = \
                    {dim_i: symbols('p_{0}_{1}_{2}___{3}'.format(curve_index,
                                                                 cp_index,
                                                                 dim_i,
                                                                 var_post_fix))
                     for dim_i in range(dimensions)}
        var_dict['p'] = state_vars
        # Define the curve
        time_variable = symbols('time_point')
        curve = [get_curve_in_dim(i, var_dict, time_variable)
                 for i in range(dimensions)]
        acc = [diff(diff(x, time_variable), time_variable) for x in curve]
        squared_acc = [x**2 for x in acc]
        sum_squared_acc = sum(squared_acc)
        cost = integrate(sum_squared_acc, (time_variable, 0, var_dict['t']))

        return cost, var_dict

    @staticmethod
    def get_multi_curve_cost_function(n_curves, dimensions, degree,
                                      start_point, end_point, var_post_fix=''):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                dimensions: The dimensionality of the search space
                degree: The degree of the bezier component bezier curves
                start_point: The start point of the problem
                end_point: The end point of the problem
                var_post_fix: A string to append to the end of variable names
            Retruns:
                symbolic formula, symbols dict
                (curve_index, {'t': time_variable,
                               'p': {point_index: {dimension_index:
                                                   point_variable}}})
                var_list: Tuple of lists defining the ordering on the variables
        '''
        start_point = {i: start_point.coords[i] for i in range(dimensions)}
        end_point = {i: end_point.coords[i] for i in range(dimensions)}
        symbols_dict = {}
        f = 0
        for curve_index in range(n_curves):
            if curve_index == 0:
                current_curve_start_point = start_point
                current_curve_zero_vel_at_start = True
            else:
                current_curve_start_point = \
                    symbols_dict[curve_index - 1]['p'][degree]
                current_curve_zero_vel_at_start = False
            if curve_index == n_curves - 1:
                current_curve_end_point = end_point
                current_curve_zero_vel_at_end = True
            else:
                current_curve_end_point = None
                current_curve_zero_vel_at_end = False

            f_curve, curve_symbols_dict = \
                BezierCurveSequence.get_cost_for_curve(
                    dimensions, degree, curve_index,
                    start_point=current_curve_start_point,
                    end_point=current_curve_end_point,
                    zero_vel_start=current_curve_zero_vel_at_start,
                    zero_vel_end=current_curve_zero_vel_at_end,
                    var_post_fix=var_post_fix)
            f += f_curve
            symbols_dict[curve_index] = curve_symbols_dict

        # Enumerate all of the variables which have been initialized

        time_variables = [symbols_dict[x]['t'] for x in symbols_dict]

        # Flatten the dict -> List
        state_variables = [list(symbols_dict[curve]['p'].values())
                           for curve in symbols_dict]
        # Flatten 2d list of dicts to 1d list of dicts
        state_variables = [item for sublist in state_variables
                           for item in sublist]
        # Flatten dicts
        state_variables = [list(x.values()) for x in state_variables]
        # Flatten 2d list to 1d
        state_variables = [item for sublist in state_variables
                           for item in sublist]
        # Remove duplicates
        state_variables = list(set(state_variables))
        # Remove non-variables, i.e. constants
        state_variables = [x for x in state_variables if type(x) is Symbol]

        return expand(f), symbols_dict, (time_variables, state_variables)

    @staticmethod
    def get_multi_curve_cost_function_matrix_form(n_curves, dimensions, degree,
                                                  start_point=None,
                                                  end_point=None,
                                                  var_post_fix=''):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                dimensions: The dimensionality of the search space
                degree: The degree of the sub bezier curves making up the
                    solution
                start_point: The start point of the problem
                end_point: The end point of the problem
                var_post_fix: A string to append to the end of variable names
            Retruns:
                P, q  where P and q are functions of
                    time_allocation
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
                var_list is a tuple of lists (time_vars, space_vars) defining
                    the ordering on the variables
        '''
        formula, symbols_dict, var_list = \
            BezierCurveSequence.get_multi_curve_cost_function(
                n_curves, dimensions, degree, start_point, end_point,
                var_post_fix)
        return BezierCurveSequence.parse_quadratic_equation_to_matrix(
            formula, symbols_dict, var_list)

    @staticmethod
    def get_l1_continuity_constraint(n_curves, dimensions, degree,
                                     symbols_dict):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                dimensions: The dimensionality of the search space
                degree: The degree of the bezier component bezier curves
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
            Returns:
                stacked equality constraints in the form h(x, y) = 0
        '''
        s = symbols_dict
        constraints = []
        for i in range(n_curves - 1):
            first_curve_i = i
            second_curve_i = i + 1
            for dimension_index in range(dimensions):
                constraints.append(
                    (s[first_curve_i]['p'][degree][dimension_index] -
                     s[first_curve_i]['p'][degree - 1][dimension_index]) /
                    s[first_curve_i]['t'] -
                    (s[second_curve_i]['p'][1][dimension_index] -
                     s[second_curve_i]['p'][0][dimension_index]) /
                    s[second_curve_i]['t']
                )
        return constraints

    @staticmethod
    def get_l2_continuity_constraint(n_curves, dimensions, degree,
                                     symbols_dict):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                dimensions: The dimensionality of the search space
                degree: The degree of the bezier component bezier curves
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
            Returns:
                stacked equality constraints in the form h(x, y) = 0
        '''
        s = symbols_dict
        constraints = []
        for i in range(n_curves - 1):
            first_curve_i = i
            second_curve_i = i + 1
            for dimension_index in range(dimensions):
                constraints.append(
                    (s[first_curve_i]['p'][degree][dimension_index] -
                     2 * s[first_curve_i]['p'][degree - 1][dimension_index] +
                     s[first_curve_i]['p'][degree - 2][dimension_index]) /
                    (s[first_curve_i]['t']**2) -
                    (s[second_curve_i]['p'][2][dimension_index] -
                     2 * s[second_curve_i]['p'][1][dimension_index] +
                     s[second_curve_i]['p'][0][dimension_index]) /
                    (s[second_curve_i]['t']**2))
        return constraints

    @staticmethod
    def get_max_vel_constraint(n_curves, dimensions, degree,
                               symbols_dict, max_vel):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                dimensions: The dimensionality of the search space
                degree: The degree of the bezier component bezier curves
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
                max_vel: The maximum velocity
            Returns:
                stacked equality constraints in the form g(x, y) <= 0
        '''
        s = symbols_dict
        constraints = []
        for i in range(n_curves):
            curve_index = i
            for dimension_index in range(dimensions):
                for control_point_index in range(degree - 1):
                    first_cp_index = control_point_index
                    second_cp_index = control_point_index + 1
                # Establish upper bound
                constraints.append(
                    (s[curve_index]['p'][first_cp_index][dimension_index] -
                     s[curve_index]['p'][second_cp_index][dimension_index]) *
                    degree/s[curve_index]['t'] - max_vel)
                # Establish lower bound
                constraints.append(
                    max_vel -
                    ((s[curve_index]['p'][first_cp_index][dimension_index] -
                     s[curve_index]['p'][second_cp_index][dimension_index]) *
                     degree/s[curve_index]['t']))
        return constraints

    @staticmethod
    def get_max_acc_constraint(n_curves, dimensions, degree,
                               symbols_dict, max_acc):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                dimensions: The dimensionality of the search space
                degree: The degree of the bezier component bezier curves
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
                max_acc: The maximum acceleration
            Returns:
                stacked equality constraints in the form g(x, y) <= 0
        '''
        s = symbols_dict
        constraints = []
        for i in range(n_curves):
            curve_index = i
            for dimension_index in range(dimensions):
                for control_point_index in range(degree - 2):
                    first_cp_index = control_point_index
                    second_cp_index = control_point_index + 1
                    third_cp_index = control_point_index + 2
                # Establish upper bound
                constraints.append(
                    (s[curve_index]['p'][first_cp_index][dimension_index] -
                     2 *
                     s[curve_index]['p'][second_cp_index][dimension_index] +
                     s[curve_index]['p'][third_cp_index][dimension_index]) *
                    (degree - 1) * degree/(s[curve_index]['t']**2) - max_acc)
                # Establish lower bound
                constraints.append(
                    max_acc -
                    ((s[curve_index]['p'][first_cp_index][dimension_index] -
                     2 *
                     s[curve_index]['p'][second_cp_index][dimension_index] +
                     s[curve_index]['p'][third_cp_index][dimension_index]) *
                     (degree - 1) * degree/(s[curve_index]['t']**2)))
        return constraints

    @staticmethod
    def polytope_containment_constraints(polytope_sequence,
                                         polytope_intersection_sequence,
                                         degree, symbols_dict):
        '''
            Args:
                polytope_sequence: [Polytope] defining the polytopes which the
                    sub-curves will pass through
                polytope_intersection_sequence: [Polytope] defining the
                    intersection polytopes which the 'hand over' points will
                    be in
                degree: The degree of the sub-bezier curves to be fitted
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
            Returns:
                stacked equality constraints in the form g(x, y) <= 0
        '''
        s = symbols_dict
        constraints = []
        for curve_index in range(len(polytope_sequence)):
            for control_point_index in range(degree + 1):
                if control_point_index != degree:
                    constraining_polytope = polytope_sequence[curve_index]
                else:
                    if control_point_index == degree:
                        try:
                            constraining_polytope = \
                                polytope_intersection_sequence[curve_index]
                        except IndexError:
                            constraining_polytope = \
                                polytope_sequence[curve_index]
                for linear_constraint in constraining_polytope.planes:
                    g = 0
                    for dimension_index in range(linear_constraint.dim):
                        g += (linear_constraint.coefficients[dimension_index] *
                              s[curve_index]['p'][control_point_index][
                              dimension_index])
                    if linear_constraint.bounds[0] == -np.inf:
                        g -= linear_constraint.bounds[1]
                    elif linear_constraint.bounds[1] == np.inf:
                        g = linear_constraint.bounds[0] - g
                    constraints.append(g)

        return constraints

    @staticmethod
    def get_equality_constraints(n_curves, dimensions, degree, symbols_dict):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                dimensions: The dimensionality of the search space
                degree: The degree of the bezier component bezier curves
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
            Returns:
                stacked equality constraints in the form h(x, y) = 0
        '''
        rtn = []
        rtn.extend(
            BezierCurveSequence.get_l1_continuity_constraint(
                n_curves, dimensions, degree, symbols_dict))
        rtn.extend(
            BezierCurveSequence.get_l2_continuity_constraint(
                n_curves, dimensions, degree, symbols_dict))
        return rtn

    @staticmethod
    def get_inequality_constraints(n_curves, dimensions, degree, symbols_dict,
                                   polytope_sequence,
                                   polytope_intersection_sequence,
                                   max_vel=3, max_acc=3):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                dimensions: The dimensionality of the search space
                degree: The degree of the bezier component bezier curves
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
                polytope_sequence: [Polytope] defining the polytopes which the
                    sub-curves will pass through
                polytope_intersection_sequence: [Polytope] defining the
                    intersection polytopes which the 'hand over' points will
                    be in
                max_vel: The value of the maximum velocity to be enforced
                max_acc: The value of the maximum acceleration to be enforced
            Returns:
                stacked equality constraints in the form h(x, y) <= 0
        '''
        rtn = []
        rtn.extend(
            BezierCurveSequence.polytope_containment_constraints(
                polytope_sequence, polytope_intersection_sequence,
                degree, symbols_dict))
        # rtn.extend(
        #     BezierCurveSequence.get_max_vel_constraint(
        #         n_curves, dimensions, degree, symbols_dict, max_vel))
        # rtn.extend(
        #     BezierCurveSequence.get_max_acc_constraint(
        #         n_curves, dimensions, degree, symbols_dict, max_acc))
        return rtn

    @staticmethod
    def get_equality_constraints_matrix_form(n_curves, dimensions, degree,
                                             symbols_dict, var_list):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                dimensions: The dimensionality of the search space
                degree: The degree of the bezier component bezier curves
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
                var_list is a tuple of lists (time_vars, space_vars) defining
                    the ordering on the variables
            Returns:
                L(y), m(y)
        '''
        stacked_equalities = BezierCurveSequence.get_equality_constraints(
            n_curves, dimensions, degree, symbols_dict)
        return BezierCurveSequence.parse_linear_equation_to_matrix(
            stacked_equalities, var_list)

    @staticmethod
    def get_inequality_constraints_matrix_form(n_curves, dimensions, degree,
                                               var_list, symbols_dict,
                                               polytope_sequence,
                                               polytope_intersection_sequence,
                                               max_vel=3, max_acc=3):
        '''
            Args:
                n_curves: The number of curves which make up the solution
                degree: The degree of the bezier component bezier curves
                symbols_dict:
                    (curve_index, {'t': time_variable,
                                   'p': {point_index: {dimension_index:
                                                       point_variable}}})
                var_list is a tuple of lists (time_vars, space_vars) defining
                    the ordering on the variables
                polytope_sequence: [Polytope] defining the polytopes which the
                    sub-curves will pass through
                polytope_intersection_sequence: [Polytope] defining the
                    intersection polytopes which the 'hand over' points will
                    be in
                max_vel: The value of the maximum velocity to be enforced
                max_acc: The value of the maximum acceleration to be enforced
            Returns:
                G(y), h(y)
        '''
        stacked_inequalities = BezierCurveSequence.get_inequality_constraints(
            n_curves, dimensions, degree, symbols_dict, polytope_sequence,
            polytope_intersection_sequence, max_vel, max_acc)
        return BezierCurveSequence.parse_linear_equation_to_matrix(
            stacked_inequalities, var_list)

    @staticmethod
    def parse_linear_equation_to_matrix(homogenious_equation_form, var_list,
                                        all_vars=False):
        '''
            Args:
                homogenious_equation_form: List of equations g which are
                    defined as ether the comnstraint g(x, y) <= 0 or
                    g(x, y) = 0.
                var_list: Tuple of lists defining the ordering on the variables
                all_vars: Boolean, True iff var list is state_vars, and not
                    (state_vars, time_vars)
            Returns:
                A(y), b(y) s.t. A(y)x <= b(y) or A(y)x = b(y) is equivelent to
                    the homogenious_equation_form as appropriate
        '''
        # Initialize the return variables
        A = []
        b = []
        if not all_vars:
            time_variables = var_list[0]
            state_variables = var_list[1]
            var = state_variables
        else:
            var = var_list
        for equation in homogenious_equation_form:
            equation = expand(equation)
            A_row = []
            b_row = []
            for state_variable in var:
                A_row.append(equation.coeff(state_variable))
            b_row.append(-equation.as_coefficients_dict()[1])
            A.append(A_row)
            b.append(b_row)

        if not all_vars:
            return (lambdify(time_variables, A, "numpy"),
                    lambdify(time_variables, b, "numpy"))
        else:
            return (A, b)

    @staticmethod
    def parse_quadratic_equation_to_matrix(equation, var_dict, variable_list):
        '''
            Args:
                equation: The equation which is the target of the transform
                var_dict: Tuple of lists defining the ordering on the variables
                variable_list: (time_variables, state_variables)
            Returns:
                A(y), b(y) s.t. 1/2*x^T*A(y)*x + x^T*b(y) + c(y), var_list

        '''
        equation = expand(equation)
        print("The equation passed in looks like:")
        print(equation)
        print("The timing variables in this case were:")
        print(variable_list[0])
        time_variables, state_variables = variable_list
        print("The start variables are:")
        print(state_variables)

        A = np.zeros((len(state_variables), len(state_variables))).tolist()
        b = np.zeros(len(state_variables)).tolist()
        c = 0
        for i in range(len(state_variables)):
            # Append the bilinear terms in x to A
            for j in range(len(state_variables)):
                if j < i:
                    continue
                elif i == j:
                    A[i][i] = 2 * equation.coeff(state_variables[i] *
                                                 state_variables[j])
                else:
                    A[i][j] = equation.coeff(state_variables[i] *
                                             state_variables[j])
                    A[j][i] = equation.coeff(state_variables[i] *
                                             state_variables[j])

            # Append the bilinear terms in x to b
            b[i] = 0
            for timing_var in time_variables:
                b[i] = equation.coeff(state_variables[i] *
                                      (timing_var**(-3))).as_coefficients_dict()[1] * timing_var**(-3)

            # print(b[i])

            #c += equation.coeff(timing_var**(-3)).as_coefficients_dict()[1] * timing_var**(-3)

            #b[i] = equation.coeff(state_variables[i]).coeff(1)

            #print(equation.coeff(state_variables[i]).as_coefficients_dict()[1])
        for timing_var in time_variables:
            c += equation.coeff(timing_var**(-3)).as_coefficients_dict()[1] * timing_var**(-3)

        print("c looks like: {}".format(c))
        return (lambdify(time_variables, A, "numpy"),
                lambdify(time_variables, b, "numpy"),
                (time_variables, state_variables),
                var_dict,
                lambdify(time_variables, c, "numpy"))


    @staticmethod
    def get_inequality_constraint_derivative(inequality_constraint_stack,
                                             time_variables, var_list):
        '''
            Args:
                ineqiuality_constraint_stack: Stack of symbolic equations
                    representing the equality constraints
                time_variables: List of the which the constraints are to be
                    derived w.r.t
                var_list: Tuple of lists defining the ordering on the variables
            Retruns:
                The derivative of the inequality_constraint_stack with respect
                    to the time_variables in the form of a function:
                    (x, y) -> Matrix, where x is the position variables and
                    y is the timing variables
        '''
        return BezierCurveSequence.matrix_derive(inequality_constraint_stack,
                                                 time_variables,
                                                 var_list)

    @staticmethod
    def get_equality_constraint_derivative(equality_constraint_stack,
                                           time_variables, var_list):
        '''
            Args:
                eqiuality_constraint_stack: Stack of symbolic equations
                    representing the equality constraints
                time_variables: List of the variables which the constraints are
                    to be derived w.r.t
                var_list: Tuple of lists defining the ordering on the variables
            Retruns:
                The derivative of the equality_constraint_stack with respect
                    to the time_variables in the form of a function:
                    (x, y) -> Matrix, where x is the position variables and
                    y is the timing variables
        '''
        return BezierCurveSequence.matrix_derive(equality_constraint_stack,
                                                 time_variables,
                                                 var_list)

    @staticmethod
    def get_objective_function_derivative(cost_function, time_variables,
                                          var_list):
        '''
            Args:
                cost_function: The cost function of the optimization
                time_variables: List of the variables which the constraints are
                    to be derived w.r.t
                var_list: Tuple of lists defining the ordering on the variables
            Retruns:
                The derivative of the cost_function with respect
                    to the time_variables in the form of a function:
                    (x, y) -> Matrix, where x is the position variables and
                    y is the timing variables
        '''
        return BezierCurveSequence.matrix_derive(cost_function,
                                                 time_variables,
                                                 var_list)

    @staticmethod
    def matrix_derive(target_object, operating_vars, var_list):
        '''
            Args:
                target_object: The formula to be derived
                operating_vars: The vector of variables to derive with respect
                    to
                var_list: Tuple of lists defining the ordering on the variables
            Retruns:
                The derivative of the object with respect
                    to the operating_variables in the form of a function:
                    (x, y) -> Matrix, where x is the position variables and
                    y is the timing variables
        '''
        result = []
        for object_row in target_object:
            result_row = []
            for diff_var in operating_vars:
                result_row.append(diff(object_row, diff_var))
            result.append(result_row)

        return lambdify(var_list, result, "numpy")

    @staticmethod
    def calc_time_alloc(start_point, end_point, intersection_polytopes,
                        velocity):
        '''
            Args:
                start_point: The start point of the search
                end_point: The end point of the search
                intersection_polytopes: A list describing the intersection
                    of the polytopes
                velocity: This argument defines the assumed average velocity
        '''

        knot_points = [start_point] + [x.mean for x in intersection_polytopes] + \
                                      [end_point]
        return [Point.distance(x, y)/velocity
                for (x, y) in zip(knot_points, knot_points[1:])]
