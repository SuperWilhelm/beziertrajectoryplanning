from geometry.point import Point
from random import shuffle
import numpy as np
import cvxpy as cp
import itertools


class Polytope:
    def __init__(self, planes):
        '''
            Args:
                planes: List of the LinearConstraint which define the boundary
                of the polytope
        '''
        self.planes = planes
        self.dim = planes[0].dim
        self.set_vertex_graph()
        self.set_mean()

    def __str__(self):
        return "{}".format([vertex.coords for vertex in self.vertices])

    def set_vertex_graph(self):
        '''
            Initializes the instance variable vertex_graph to a
                dict describing the connections between vertices
                given the planes
        '''
        all_intersections = []
        # Consider all subsets of self.planes and of size self.dim
        # And store result in all_intersections if this system of equations
        # has a unitue solution
        for plane_subset in itertools.combinations(self.planes,
                                                   self.dim):
            gradient_array = []
            constant_array = []
            for plane in list(plane_subset):
                gradient_array.append(plane.coefficients)
                constant_array.append(plane.constant)

            # convert the plane arrays into np.arrays and solve
            gradient_array = np.array(gradient_array)
            constant_array = np.array(constant_array)
            try:
                all_intersections.append(
                    (plane_subset, Point(np.linalg.solve(gradient_array,
                                                         constant_array))))
            except np.linalg.LinAlgError:
                # The system of equations did not have a unique intersection
                pass

        # Filter the points of intersection to only include the points which
        # lie on the shape
        vertices = [x for x in all_intersections if self.contains_point(x[1])]

        # Parse the list of (hyperplane_set, point) into a graph of points
        # where there is an undirected edge between two vertices iff they are
        # connected (i.e. vary by one plane). Key will be point and value will
        # be list of neighbouring points
        vertex_graph = {x[1]: [] for x in vertices}

        for vertex in vertices:
            for neighbour_vertex_candidate in vertices:
                vary_by_one_plane = len(set(neighbour_vertex_candidate[0]) &
                                        set(vertex[0])) == self.dim - 1
                if vary_by_one_plane:
                    # Points vary by one plane
                    vertex_graph[vertex[1]].append(neighbour_vertex_candidate)
        self.vertex_graph = vertex_graph
        if self.dim != 2:
            self.vertices = list(vertex_graph.keys())
        elif list(vertex_graph.keys()) == []:
            self.vertices = []
        else:
            self.vertices = [list(vertex_graph.keys())[0]]
            while True:
                current_vertex = self.vertices[-1]
                next_unvisited_vertex = [x for x in
                                         self.vertex_graph[current_vertex]
                                         if x[1] not in self.vertices]

                if next_unvisited_vertex == []:
                    break
                else:
                    self.vertices.append(next_unvisited_vertex[-1][1])

    def set_mean(self):
        '''
            Initializes the instance variable mean_value which
            Point which is the average of the vertices
        '''
        if self.vertices != []:
            self.mean = Point.mean(self.vertices)
        else:
            self.mean = None

    def random_point(self):
        '''
            Returns:
                A random point inside the polytope
        '''

        weights = np.random.dirichlet(
            np.ones(len(self.vertices)-1), size=1).tolist()

        weights = [x*0.1 for x in weights[0]] + [0.9]
        shuffle(weights)
        return sum([self.vertices[i]*weights[i] for i in range(len(weights))])

    def contains_point(self, point):
        '''
            Args:
                point: A query point
            Returns:
                boolean: True iff the query point is contained in
                    or is on the boundary of the the polytope
        '''
        return np.all([plane.point_valid(point)
                       for plane in self.planes])

    def is_non_empty(self):
        return self.contains_point(self.mean)

    def variable_containment_constraints(self, variable):
        '''
            Args:
                variable: cvxpy.variable to constrain inside the polytope
            Returns:
                list of linear constraints defining when a variable is inside
                    of the polytope
        '''
        return [plane.variable_respecting_constraint(variable)
                for plane in self.planes]

    @staticmethod
    def get_points_minimizing_distance(start_point, end_point,
                                       polytope_sequence,
                                       polytope_sequence_intersections):
        '''
            Args:
                start_point: The start point of the search
                end_point: The end point of the search
                polytope_sequence: The sequence of polytopes which the solution
                    to the search will be constrained inside
                polytope_sequence_intersections: List of polytopes describing
                    the intersection of the polytope_sequence
            Returns:
                dict indexed by polytope and value dict indexed by "start",
                "end" and value Point, describing the start and end points for
                each polytope which minimizes the least squared value
        '''
        dim = start_point.dim
        n = len(polytope_sequence_intersections)
        # Create the variables of optimization i.e. the points
        x = [cp.Variable(dim) for _ in range(n)]
        # Add in the fixed points in the appropriate place
        x = [start_point.coords] + x + [end_point.coords]

        constraints = []
        objective = 0

        for i in range(len(polytope_sequence_intersections) + 1):
            # i represents the index of the 'leg'
            # Constrain x[i] to be inside polytope_sequence_intersections[i]
            if i != len(polytope_sequence_intersections):
                constraining_polytope = polytope_sequence_intersections[i]
                constraints.extend(
                    constraining_polytope.variable_containment_constraints(
                        x[i+1]))

            # Add the euclidean distance from poin
            objective += cp.norm(x[i] - x[i+1])

        objective = cp.Minimize(objective)

        prob = cp.Problem(objective, constraints)
        prob.solve()

        points = [start_point]
        for variable in prob.variables():
            points.append(Point(variable.value))

        points.append(end_point)

        result_dict = {}
        for polytope_index in range(len(polytope_sequence)):
            polytope = polytope_sequence[polytope_index]
            result_dict[polytope] = \
                {"start": points[polytope_index],
                 "end": points[polytope_index + 1]}
        return result_dict

    def __lt__(self, other):
        return 1

    def __le__(self, other):
        return 1
