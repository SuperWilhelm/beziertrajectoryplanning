import numpy as np


class LinearConstraint:
    def __init__(self, coefficients, constant, valid_point):
        '''
            Args:

                coefficients: n dimensional list with the plane
                    coefficients
                constant: The constant of the plane equation
                valid_point: A Point which is valid w.r.t the constraint
        '''
        self.dim = len(coefficients)
        self.coefficients = coefficients
        self.constant = constant
        self.normal_vector = coefficients/np.linalg.norm(coefficients)
        self.valid_point = valid_point
        self.set_bounds()

    def set_bounds(self):
        '''
            Args:
                valid_point: A point which lies on the valid side
                    of the plane
            Returns:
                List [lower, upper] of 2 Real Numbers giving bounds
                s.t. the expression
                lower<= point<dot>coefficients - constant <= upper
                is valid iff the point is valid w.r.t the PlainConstraint
                None if the constraint is unbounded in a direction then
                -np.inf or np.inf will be used as appropriate
        '''
        # evaluate the valid point
        legal_value = sum([x*y for x, y in zip(self.coefficients,
                                               self.valid_point.coords)])
        if legal_value < self.constant:
            self.bounds = [-np.inf, self.constant]
        elif legal_value > self.constant:
            self.bounds = [self.constant, np.inf]
        else:
            # No inference can be made about what side of the plane is valid
            raise ValueError("The example valid point lies on the plane")

    def point_valid(self, point):
        '''
            Args:
                point: The query point
            Return:
                True iff the point is valid w.r.t this constraint
        '''
        if point is None:
            return False
        point_value = sum([x*y for x, y in zip(self.coefficients,
                                               point.coords)])
        return self.bounds[0] <= point_value and \
            self.bounds[1] >= point_value

    def variable_respecting_constraint(self, variable):
        '''
            Args:
                variable: cvxpy.variable which is to be constrained
                    correctly with respect to self
            Returns:
                Constraint for the variable which defines respecting
                    self
        '''
        if self.bounds[0] == -np.inf:
            # The upper bound is the one with "weight"
            return np.array(self.coefficients)@variable <= self.bounds[1]
        elif self.bounds[1] == np.inf:
            # The lower bound is the one with "weight"
            return np.array(self.coefficients)@variable >= self.bounds[0]
        else:
            # self is ill-defined
            raise Exception("This linear constraint is ill-defined")
