import cvxpy as cp
import numpy as np
from geometry.bezier_curve_sequence import BezierCurveSequence
from sympy.core.symbol import Symbol
from geometry.bezier_curve import BezierCurve
from geometry.point import Point


class QuadraticProgramMultiPolytopeSolver:
    def __init__(self, problem, time_allocation=None):
        '''
            Args:
                problem: The bezier path problem to be solved
        '''
        self.problem = problem
        self.time_allocation = time_allocation

    def solve(self):
        '''
            Args:
                time_allocation: The allocation of time for each of the curves
            Returns:
                Solution: (BezierCurveSequence describing the solution,
                           [Polytope] describing the polytopes which the
                           solution travels through)
        '''
        # Firstly select a sequence of polytopes from the convex free space
        # from the start point leading to the goal
        shortest_path = \
            self.problem.convex_free_space.get_shortest_path(
                self.problem.start_point,
                self.problem.end_point)

        self.polytopes = [x[0] for x in shortest_path][:-1]
        self.n_curves = len(self.polytopes)
        self.adjacent_polytopes = list(zip(self.polytopes,
                                           self.polytopes[1:]))

        self.intersection_polytopes = [
            self.problem.convex_free_space.intersection_graph[poly_0][poly_1]
            for (poly_0, poly_1) in self.adjacent_polytopes]

        self.P, self.q, self.var_list, self.var_dict, self.c = \
            self.get_cost_function()
        self.G, self.h = self.get_inequality_constraints()
        self.L, self.m = self.get_equality_constraints()
        if self.time_allocation is None:
            self.time_allocation = BezierCurveSequence.calc_time_alloc(
                self.problem.start_point, self.problem.end_point,
                self.intersection_polytopes, self.problem.average_vel_heuristic)

        print("The duration of this curve is: {}".format(
            self.time_allocation))

        # Fix the time allocarion
        self.P_t, self.q_t = (self.P(*self.time_allocation),
                              self.q(*self.time_allocation))
        self.G_t, self.h_t = (self.G(*self.time_allocation),
                              self.h(*self.time_allocation))
        self.L_t, self.m_t = (self.L(*self.time_allocation),
                              self.m(*self.time_allocation))

        self.c_t = self.c(*self.time_allocation)

        self.P_t = np.array(self.P_t)
        self.q_t = np.array(self.q_t)
        self.G_t = np.array(self.G_t)
        self.h_t = np.array(self.h_t)
        self.L_t = np.array(self.L_t)
        self.m_t = np.array(self.m_t)

        self.h_t = np.asarray(self.h_t).reshape(-1)
        self.m_t = np.asarray(self.m_t).reshape(-1)

        n = len(self.var_list[1])

        x = cp.Variable(n)

        # (1/2)*cp.quad_form(x, self.P_t) + self.q_t.T@x
        # The constraints are contradictory!
        # The set of constraints are the:
        #     - Polytope containment constraints
        #     - Continuity constraints
        #
        prob = cp.Problem(cp.Minimize(
                   (1/2)*cp.quad_form(x, self.P_t) + self.q_t.T@x + self.c_t),
                   [self.G_t@x <= self.h_t,
                    self.L_t@x == self.m_t])
        print(prob.solve())

        return (self.polytopes,
                BezierCurveSequence(
                    self.parse_solution_to_bezier_sequence(x.value)),
                prob.value, prob.solver_stats.solve_time)

    def get_cost_function(self):
        '''
            Returns:
                The cost function for the problem in quadratic form P, q
                    in addition to a dict mapping
                    (curve_index, dimension_index, control_point_index)
                        -> variable index in the search vector
                and the variable_list
        '''
        return BezierCurveSequence.get_multi_curve_cost_function_matrix_form(
            self.n_curves, self.problem.convex_free_space.dim,
            self.problem.bezier_degree, start_point=self.problem.start_point,
            end_point=self.problem.end_point)

    def get_inequality_constraints(self):
        '''
            Returns:
                The inequality constraints of the problem in G, h matrix form
        '''
        return BezierCurveSequence.get_inequality_constraints_matrix_form(
            self.n_curves, self.problem.convex_free_space.dim,
            self.problem.bezier_degree,  self.var_list, self.var_dict,
            self.polytopes, self.intersection_polytopes)

    def get_equality_constraints(self):
        '''
            Returns:
                The inequality constraints of the problem in L, m matrix form
        '''
        return BezierCurveSequence.get_equality_constraints_matrix_form(
            self.n_curves, self.problem.convex_free_space.dim,
            self.problem.bezier_degree,  self.var_dict, self.var_list)

    def parse_solution_to_bezier_sequence(self, solution_vector):
        '''
            Args:
                solution_vector
            Returns:
                [BezierCurve]
        '''
        def control_point_lookup(curve_index, control_point_index,
                                 dimension_index):
            '''
                Args:
                    curve_index: The index of the curve in the solution
                    control_point_index: The index of the control point
                    dimension_index: The dimension of the index
                Returns:
                    real number to the value described in the argument's
                        of the solution curve
            '''
            cp_index = control_point_index
            dim_index = dimension_index
            value = self.var_dict[curve_index]['p'][cp_index][dim_index]
            if type(value) is not Symbol:
                return value
            else:
                var_index = self.var_list[1].index(value)
                return solution_vector[var_index]

        rtn = []
        for curve_index in range(self.n_curves):
            curve_time = self.time_allocation[curve_index]
            control_points = []
            for control_point_index in range(self.problem.bezier_degree + 1):
                point = []
                for dim_index in range(self.problem.dim):
                    point.append(control_point_lookup(curve_index,
                                                      control_point_index,
                                                      dim_index))
                control_points.append(Point(point))
            rtn.append(BezierCurve(control_points, curve_time))
        return rtn
