import cvxpy as cp
import numpy as np
from geometry.bezier_curve_sequence import BezierCurveSequence
from sympy.core.symbol import Symbol
# Some of the code in this file is inspired from the documentation at:
# https://www.cvxpy.org/examples/basic/sdp.html


class SemiDefiniteProgram:
    def __init__(self, problem):
        '''
            Args:
                problem: A BezierPathProblem to be solved
        '''
        raise NotImplementedError
        self.problem = problem

    def solve(self):
        '''
            This function when called will solve self.problem using
            Semi-Definite programming.
            Returns:
                Solution: [BezierCurve]
        '''
        # Firstly select a sequence of polytopes from the convex free space
        # from the start point leading to the goal
        shortest_path = \
            self.problem.convex_free_space.get_shortest_path(
                self.problem.start_point,
                self.problem.end_point)

        self.polytopes = [x[0] for x in shortest_path][:-1]
        self.n_curves = len(self.polytopes)
        self.intersection_polytopes = [x[2] for x in shortest_path if x[2]
                                       is not None]

        self.C, self.var_grid, self.var_dict = self.get_cost_function()
        self.A, self.b = self.get_inequality_constraints()
        self.D, self.e = self.get_equality_constraints()

        n = len(self.var_grid)

        # Define and solve the CVXPY problem.
        # Create a symmetric matrix variable.
        X = cp.Variable((n, n), symmetric=True)
        # Add the constraint that the optimization matrix is Positive-Semi
        # Definite
        constraints = [X >> 0]
        constraints += [cp.trace(self.A[i] @ X) <= self.b[i]
                        for i in range(len(self.A))]

        constraints += [cp.trace(self.D[i] @ X) == self.e[i]
                        for i in range(len(self.D))]

        prob = cp.Problem(cp.Minimize(cp.trace(self.C @ X)),
                          constraints)
        prob.solve()

        return self.convert_solution_to_bezier_sequence()

    def get_cost_function(self):
        '''
            Returns:
                Matrix describing the cost, w.r.t. matrix inner product with
                    optimization vector
        '''
        # Get the cost function in symbolic form 
        cost, var_dict, var_list = \
            BezierCurveSequence.get_multi_curve_cost_function(
                self.n_curves, self.problem.dim, self.problem.bezier_degree,
                self.problem.start_point, self.problem.end_point)

        time_variables = [var_dict[x]['t'] for x in var_dict]

        # Flatten the dict -> List
        state_variables = [list(var_dict[curve]['p'].values())
                           for curve in var_dict]
        # Flatten 2d list of dicts to 1d list of dicts
        state_variables = [item for sublist in state_variables
                           for item in sublist]
        # Flatten dicts
        state_variables = [list(x.values()) for x in state_variables]
        # Flatten 2d list to 1d
        state_variables = [item for sublist in state_variables
                           for item in sublist]
        # Remove duplicates
        state_variables = list(set(state_variables))
        # Remove non-variables, i.e. constants
        state_variables = [x for x in state_variables if type(x) is Symbol]

        return A, var_dict, var_grid

    def get_equality_constraints(self):
        '''
            Returns:
                Matrix describing the equality constraints, w.r.t. matrix inner
                    product with optimization vector
        '''
        raise NotImplementedError("This function is yet to be implemented")

    def get_inequality_constraints(self):
        '''
            Returns:
                Matrix describing the inequality constraints, w.r.t. matrix
                    inner product with optimization vector
        '''
        raise NotImplementedError("This function is yet to be implemented")

    def convert_solution_to_bezier_sequence(self):
        '''
            Retruns:
                solution: [BezierCurve] describing the optimal solution found
        '''
        raise NotImplementedError("This function is yet to be implemented")