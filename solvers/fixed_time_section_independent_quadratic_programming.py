from solvers.single_polytope_solvers.quadratic_program_solver import \
                                                QuadraticProgramSolver
from geometry.bezier_curve_sequence import BezierCurveSequence
from geometry.polytope import Polytope
from geometry.point import Point


class HeuristicTimeQuadraticProgrammingSectionIndependent:
    def __init__(self, problem):
        '''
            Args:
                problem: The bezier path problem to be solved
                average_vel_heuristic: The valie of the average speed when
                    calculating the time allocation heuristic
                zero_vel_at_ends: True if and only if zero velocity constraints
                    are to be enforced at the end points of the sub-curves

        '''
        self.problem = problem

    def solve(self):
        '''
            This function when called will solve self.problem by
                - Selecting a sequence of polytopes
                - Finding knot-points in the intersection of solution
                  polytopes which minimize least squares
                - Solve for the optimal trajectory in each polytope
            Returns:
                [Polytope], [BezierCurve], solve_time, solve_cost
        '''
        # Firstly select a sequence of polytopes from the convex free space
        # from the start point leading to the goal
        shortest_path = \
            self.problem.convex_free_space.get_shortest_path(
                self.problem.start_point,
                self.problem.end_point)

        polytopes = [x[0] for x in shortest_path][:-1]
        adjacent_polytopes = list(zip(polytopes,
                                      polytopes[1:]))

        intersection_polytopes = [
            self.problem.convex_free_space.intersection_graph[poly_0][poly_1]
            for (poly_0, poly_1) in adjacent_polytopes]

        self.knot_points = Polytope.get_points_minimizing_distance(
            self.problem.start_point, self.problem.end_point,
            polytopes, intersection_polytopes)

        time_allocations = [Point.distance(self.knot_points[x]['start'],
                                           self.knot_points[x]['end']) /
                            self.problem.average_vel_heuristic
                            for x in self.knot_points]

        print("The time allocations looked like: {}".format(
            time_allocations))

        # Initialize the solvers for each curve
        solvers = []
        for polytope_index in range(len(polytopes)):
            polytope = polytopes[polytope_index]
            start_point = self.knot_points[polytope]['start']
            end_point = self.knot_points[polytope]['end']
            time_allocation = time_allocations[polytope_index]
            qp_solver = QuadraticProgramSolver(
                polytope, start_point, end_point, time_allocation,
                self.problem.bezier_degree, max_vel=self.problem.max_vel,
                max_acc=self.problem.max_acc)
            solvers.append(qp_solver)
        # Bring together the results from each of the solvers
        bezier_curves = []
        total_time = 0
        total_cost = 0
        for solver in solvers:
            curve, curve_cost, solve_time = solver.solve()
            bezier_curves.append(curve)
            total_time += solve_time
            total_cost += curve_cost
        return (polytopes, BezierCurveSequence(bezier_curves),
                total_time, total_cost)
