from geometry.bezier_curve import BezierCurve
from geometry.point import Point
from geometry.bezier_curve_sequence import BezierCurveSequence
from scipy.optimize import Bounds, LinearConstraint, NonlinearConstraint, \
                           minimize
from sympy import lambdify, diff
from sympy.core.symbol import Symbol
from timeit import default_timer as timer

import numpy as np


class NonConvexTrustRegion:
    def __init__(self, problem):
        '''
            Args:
                problem: A BezierPathProblem to be solved
        '''
        self.problem = problem

    def solve(self):
        '''
            Args:
                time_allocation: The allocation of time for each of the curves
            Returns:
                Solution: (BezierCurveSequence describing the solution,
                           [Polytope] describing the polytopes which the
                           solution travels through)
        '''
        # Firstly select a sequence of polytopes from the convex free space
        # from the start point leading to the goal
        shortest_path = \
            self.problem.convex_free_space.get_shortest_path(
                self.problem.start_point,
                self.problem.end_point)

        self.polytopes = [x[0] for x in shortest_path][:-1]
        self.n_curves = len(self.polytopes)

        adjacent_polytopes = list(zip(self.polytopes,
                                      self.polytopes[1:]))

        self.intersection_polytopes = [
            self.problem.convex_free_space.intersection_graph[poly_0][poly_1]
            for (poly_0, poly_1) in adjacent_polytopes]

        self.cost, self.var_dict, self.var_list = \
            BezierCurveSequence.get_multi_curve_cost_function(
                self.n_curves, self.problem.dim, self.problem.bezier_degree,
                self.problem.start_point, self.problem.end_point)

        self.all_var_list = self.var_list[0] + self.var_list[1]

        self.bounds = self.get_bounds()
        self.linear_constraint = self.get_linear_constraints()
        self.non_linear_constraint = self.get_non_linear_constraints()

        self.cost_jac, self.cost_hess = self.get_derivatives()
        self.cost = lambdify(np.array([self.all_var_list]), self.cost, "numpy")

        start = timer()
        result = minimize(self.cost, [1]*len(self.all_var_list), method='trust-constr',
                          jac=self.cost_jac, hess=self.cost_hess,
                          constraints=[self.linear_constraint,
                                       self.non_linear_constraint],
                          bounds=self.bounds)

        end = timer()

        return (self.polytopes, self.parse_solution_to_bezier(result.x),
                self.cost(result.x), end-start)

    def get_bounds(self):
        '''
            Returns:
               Bounds on the variables of optimization
        '''
        return Bounds(
            len(self.var_list[0])*[0] + len(self.var_list[1])*[-np.inf],
            len(self.var_list[0])*[np.inf] + len(self.var_list[1])*[np.inf])

    def get_linear_constraints(self):
        '''
            Returns:
                LinearConstraint describing the control point containment
        '''
        control_point_containment = \
            BezierCurveSequence.polytope_containment_constraints(
                self.polytopes, self.intersection_polytopes,
                self.problem.bezier_degree, self.var_dict)

        A, b = BezierCurveSequence.parse_linear_equation_to_matrix(
            control_point_containment, self.all_var_list, True)

        b = [x[0] for x in b]

        new_b = []

        for x in b:
            new_b.append(float(x))

        new_A = []

        for row in A:
            new_row = []
            for element in row:
                new_row.append(float(element))
            new_A.append(new_row)

        return LinearConstraint(new_A, -np.inf, new_b)

    def get_non_linear_constraints(self):
        '''
            Returns:
                NonlinearConstraint describing the max vel/acc constraint
                continuity constraints
        '''
        # Get the first and second order continuity constraints
        constraints = []
        constraints.extend(BezierCurveSequence.get_l1_continuity_constraint(
            self.n_curves, self.problem.dim, self.problem.bezier_degree,
            self.var_dict))

        constraints.extend(BezierCurveSequence.get_l2_continuity_constraint(
            self.n_curves, self.problem.dim, self.problem.bezier_degree,
            self.var_dict))

        lower_bounds = [0]*len(constraints)
        upper_bounds = [0]*len(constraints)

        # Now add the maximum velocity and maximum acceleration constraints
        inequality_constraints = []
        inequality_constraints.extend(
            BezierCurveSequence.get_max_vel_constraint(
                self.n_curves, self.problem.dim, self.problem.bezier_degree,
                self.var_dict, 3))

        inequality_constraints.extend(
            BezierCurveSequence.get_max_acc_constraint(
                self.n_curves, self.problem.dim, self.problem.bezier_degree,
                self.var_dict, 3))

        inequality_lower_bounds = [-np.inf]*len(inequality_constraints)
        inequality_upper_bounds = [0]*len(inequality_constraints)

        constraints.extend(inequality_constraints)
        lower_bounds.extend(inequality_lower_bounds)
        upper_bounds.extend(inequality_upper_bounds)

        cons_f = lambdify(np.array([self.all_var_list]), constraints, "numpy")

        derivatives = [[diff(constraint, var) for var in self.all_var_list]
                       for constraint in constraints]

        cons_J = lambdify(np.array([self.all_var_list]), derivatives, "numpy")

        double_derivatives = [[[diff(var_div, var)
                                for var in self.all_var_list]
                               for var_div in derivative]
                              for derivative in derivatives]

        double_derivatives = [lambdify(np.array([self.all_var_list]), x, "numpy")
                              for x in double_derivatives]

        def cons_H(x, v):
            return sum([v[i] * np.array(double_derivatives[i](x))
                        for i in range(len(constraints))])

        return NonlinearConstraint(cons_f, lower_bounds, upper_bounds,
                                   jac=cons_J, hess=cons_H)

    def get_derivatives(self):
        '''
            Returns:
                (Jacobian of the cost function,
                 Hessian of the cost function)
        '''
        derivatives = [diff(self.cost, var) for var in self.all_var_list]

        cost_J = lambdify(np.array([self.all_var_list]), derivatives, "numpy")

        double_derivatives = [[diff(der, var) for var in self.all_var_list]
                              for der in derivatives]

        cost_H = lambdify(np.array([self.all_var_list]),
                          double_derivatives, "numpy")

        return cost_J, cost_H

    def parse_solution_to_bezier(self, solution_vector):
        '''
            Args:
                solution_vector
            Returns:
                BezierCurveSequence
        '''
        def control_point_lookup(curve_index,
                                 control_point_index,
                                 dimension_index):
            '''
                Args:
                    curve_index: The index of the curve
                    control_point_index: The index of the control point
                    dimension_index: The dimension of the index
                Returns:
                    real number to the value described in the argument's
                        of the solution curve
            '''
            cp_index = control_point_index
            dim_index = dimension_index
            value = self.var_dict[curve_index]['p'][cp_index][dim_index]
            if type(value) is not Symbol:
                return value
            else:
                var_index = self.all_var_list.index(value)
                return solution_vector[var_index]

        def time_lookup(curve_index):
            '''
                Args:
                    curve_index: The index of the curve who's time is to be
                        queried.
            '''
            return solution_vector[curve_index]

        bezier_curve_list = []
        for bezier_curve_index in range(self.n_curves):
            curve_time = time_lookup(bezier_curve_index)
            control_point_list = []
            # Now that the curve time has been obtained, obtain the state vars
            for control_point_index in range(self.problem.bezier_degree + 1):
                control_point_coords = []
                for dim_index in range(self.problem.dim):
                    control_point_coords.append(
                        control_point_lookup(bezier_curve_index,
                                             control_point_index,
                                             dim_index))
                control_point_list.append(Point(control_point_coords))
            bezier_curve_list.append(BezierCurve(control_point_list, curve_time))

        return BezierCurveSequence(bezier_curve_list)
