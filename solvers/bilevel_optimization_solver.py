from geometry.bezier_curve_sequence import BezierCurveSequence
from solvers.quadratic_program_multi_polytope_solver import \
    QuadraticProgramMultiPolytopeSolver
from scipy.optimize import line_search
import numpy as np
import cvxpy as cp


class BilevelOptimization:
    '''
        This implementation is based on the method outlined in:
            https://arxiv.org/pdf/1811.10753.pdf
        For the above search method, the line search component will
            be implemented using scipy.optimize.line_search
    '''
    def __init__(self, problem):
        '''
            Args:
                problem: A BezierPathProblem to be solved
        '''
        self.problem = problem

    def solve(self):
        '''
            This function when called will solve self.problem using
            a method of a bilevel optimization
            Returns:
        '''
        # Find the shortest path
        shortest_path = \
            self.problem.convex_free_space.get_shortest_path(
                self.problem.start_point,
                self.problem.end_point)

        # Define variables to store details on the path of polytopes to take
        self.polytopes = [x[0] for x in shortest_path][:-1]
        self.n_curves = len(self.polytopes)
        self.adjacent_polytopes = list(zip(self.polytopes,
                                           self.polytopes[1:]))

        self.intersection_polytopes = [
            self.problem.convex_free_space.intersection_graph[poly_0][poly_1]
            for (poly_0, poly_1) in self.adjacent_polytopes]

        self.x0 = BezierCurveSequence.calc_time_alloc(
            self.problem.start_point, self.problem.end_point,
            self.intersection_polytopes, self.problem.average_vel_heuristic)

        # Now obtain the relevant cost function and constraints
        self.P, self.q, self.func_cost, self.var_list, self.var_dict, self.c = \
            self.get_cost_function()

        # Get the constraint functions
        self.G, self.h, self.func_g = self.get_inequality_constraints()
        self.L, self.m, self.func_h = self.get_equality_constraints()

        self.func_cost_prime = \
            BezierCurveSequence.matrix_derive([self.func_cost],
                                              self.var_list[0],
                                              self.var_list)

        self.func_g_prime = BezierCurveSequence.matrix_derive(self.func_g,
                                                              self.var_list[0],
                                                              self.var_list)

        self.func_h_prime = BezierCurveSequence.matrix_derive(self.func_h,
                                                              self.var_list[0],
                                                              self.var_list)

        self.best_time_alloc = self.x0
        self.best_time_alloc_value = None
        self.best_time_alloc_value = self.solve_qp(self.x0)[3]

        self.refine_time()

        print("The best time alloc was: {}".format(self.best_time_alloc))
        print("The time alloc value for this was: {}".format(self.best_time_alloc_value))

        solver = QuadraticProgramMultiPolytopeSolver(self.problem, self.best_time_alloc)

        return solver.solve()


    def get_cost_function(self):
        '''
            Returns:
                The cost function of the problem in the form:
                (P, q, symbolic_cost_function, var_list, var_dict)
        '''
        func = BezierCurveSequence.get_multi_curve_cost_function(
            self.n_curves, self.problem.convex_free_space.dim,
            self.problem.bezier_degree, start_point=self.problem.start_point,
            end_point=self.problem.end_point)

        P, q, var_list, var_dict, c = \
            BezierCurveSequence.parse_quadratic_equation_to_matrix(*func)

        return P, q, func[0], var_list, var_dict, c

    def get_inequality_constraints(self):
        '''
            Returns:
                The inequality constraints of the problem in G, h matrix form
        '''
        func = BezierCurveSequence.get_inequality_constraints(
            self.n_curves, self.problem.convex_free_space.dim,
            self.problem.bezier_degree,  self.var_dict,
            self.polytopes, self.intersection_polytopes)

        A, b = BezierCurveSequence.parse_linear_equation_to_matrix(
            func, self.var_list)

        return A, b, func

    def get_equality_constraints(self):
        '''
            Returns:
                The inequality constraints of the problem in L, m matrix form
        '''
        func = BezierCurveSequence.get_equality_constraints(
            self.n_curves, self.problem.convex_free_space.dim,
            self.problem.bezier_degree,  self.var_dict)

        A, b = BezierCurveSequence.parse_linear_equation_to_matrix(
            func, self.var_list)

        return A, b, func

    def refine_time(self):
        '''
            Utilizes the class variables:
                self.time_cost: The cost function for each time allocation
                self.time_cost_grad: The function giving the gradient of the
                    self.time_cost
        '''
        x0_grad = self.time_cost_grad(self.x0)

        return line_search(self.time_cost, self.time_cost_grad,
                           self.x0, x0_grad)

    def solve_qp(self, time_allocation):
        '''
            Args:
                time_allocation: The time allocation to solve the problem with
            Returns:
                x_val, lambda, mu (Lagrange Multipliers)
        '''
        print("Solving the quadratic program with time time allocation: {}".format(
            time_allocation))
        # Fix the time allocarion
        P_t, q_t = (self.P(*time_allocation),
                    self.q(*time_allocation))
        c_t = self.c(*time_allocation)
        G_t, h_t = (self.G(*time_allocation),
                    self.h(*time_allocation))
        L_t, m_t = (self.L(*time_allocation),
                    self.m(*time_allocation))

        P_t = np.array(P_t)
        q_t = np.array(q_t)
        G_t = np.array(G_t)
        h_t = np.array(h_t)
        L_t = np.array(L_t)
        m_t = np.array(m_t)

        h_t = np.asarray(h_t).reshape(-1)
        m_t = np.asarray(m_t).reshape(-1)

        n = len(self.var_list[1])

        x = cp.Variable(n)

        prob = cp.Problem(cp.Minimize(
                   (1/2)*cp.quad_form(x, P_t) + q_t.T@x +c_t),
                   [G_t@x <= h_t,
                    L_t@x == m_t])
        try:
            val = prob.solve()
            if self.best_time_alloc_value is None or \
                    val < self.best_time_alloc_value:
                self.best_time_alloc_value = val
                self.best_time_alloc = time_allocation
        except:
            return None

        return (x.value,
                prob.constraints[0].dual_value,
                prob.constraints[1].dual_value,
                prob.value)

    def get_gradient(self, x_opt, lamb, mu, y):
        '''
            Args:
                x_opt: The optimal values for the state variables
                lam: The Lagrange Multiplier corresponding to the equality
                    constraints
                mu: The lagrange Multiplier corresponding to the inequality
                    constraints
                y: The location to evaluate the gradient (timing variables )
            Retruns:
                Gradient of the objective function with respect to the time
                    variables
            More Info:
            https://arxiv.org/pdf/1811.10753.pdf - Eq 13
        '''
        lamb = np.array(lamb)
        mu = np.array(mu)
        cost = np.array(self.func_cost_prime(y, x_opt))
        g_prime = np.array(self.func_g_prime(y, x_opt))
        h_prime = np.array(self.func_h_prime(y, x_opt))

        dir_vect = lamb.T.dot(g_prime) + mu.T.dot(h_prime) + cost

        return dir_vect/np.sqrt((dir_vect**2).sum())

    def time_cost(self, time_allocation):
        '''
            Args:
                time_allocation: ndarray, desctibing the timing vector
            Returns:
                cost of associated with the time_allocation and corresponding
                    optimal state variables
        '''
        qp_result = self.solve_qp(time_allocation)
        if qp_result is None:
            return np.inf
        else:
            return qp_result[3]

    def time_cost_grad(self, time_allocation):
        '''
            Args:
                time_allocation: ndarray,describing the timing vector
            Returns:
                gradient of the cost funtion vector
        '''
        qp_result = self.solve_qp(time_allocation)
        if qp_result is None:
            return [0]*self.n_curves
        x_opt, lamb, mu, _ = qp_result
        rtn = -1*self.get_gradient(x_opt, lamb, mu, time_allocation)[0]
        return rtn
