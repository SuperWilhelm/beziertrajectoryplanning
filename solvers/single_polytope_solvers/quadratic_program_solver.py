import cvxpy as cp
import numpy as np
from sympy.core.symbol import Symbol
from geometry.bezier_curve import BezierCurve
from geometry.bezier_curve_sequence import BezierCurveSequence
from geometry.point import Point


class QuadraticProgramSolver:
    def __init__(self, polytope, start_point, end_point, time_allocation,
                 bezier_degree, max_vel=None, max_acc=None):
        '''
            Args:
                polytope: The polytope in which the search is to be within
                start_point: The point inside the polytope where the curve
                    will start
                end_point: The point inside the polytope which the curve will
                    end
                time_allocation: The time allocated to the curve
                bezier_degree: The degree of the bezier to be solved for
                max_vel: The maximum velocity constraint
                max_acc: The maximum acceleration constraint
            Returns:
                BezierCurve describing the optimal solution
        '''
        self.polytope = polytope
        self.start_point = start_point
        self.end_point = end_point
        self.time_allocation = time_allocation
        self.bezier_degree = bezier_degree
        self.dim = polytope.dim
        self.max_vel = max_vel
        self.max_acc = max_acc
        self.P, self.q, self.var_list, self.symbols_dict, self.c = \
            BezierCurveSequence.get_multi_curve_cost_function_matrix_form(
                1, self.dim, self.bezier_degree, start_point=start_point,
                end_point=end_point)
        self.G, self.h = \
            BezierCurveSequence.get_inequality_constraints_matrix_form(
                1, self.dim, self.bezier_degree, self.var_list,
                self.symbols_dict, [polytope], [],
                max_vel, max_acc)

        self.P, self.q = (np.array(self.P(self.time_allocation)),
                          np.array(self.q(self.time_allocation)))

        self.G, self.h = (np.array(self.G(self.time_allocation)),
                          np.array(self.h(self.time_allocation)))

        self.c = np.array(self.c(self.time_allocation))

        self.h = self.h.reshape((self.h.shape[0],))

    def solve(self):
        '''
            Returns:
                BezierCurve defining the solution curve,

        '''
        n = len(self.var_list[1])
        x = cp.Variable(n)
        prob = cp.Problem(cp.Minimize(
                   (1/2)*cp.quad_form(x, self.P) + self.q.T@x + self.c),
                   [self.G@x <= self.h])
        value = prob.solve()

        return self.parse_solution_from_vector_to_bezier(
            x.value), prob.solver_stats.solve_time, value

    def parse_solution_from_vector_to_bezier(self, vector):
        '''
            Args:
                vector of variables in the ordered firstly by control point
                    index and then by dimension index
        '''
        def var_lookup(control_point_index, dimension_index):
            dict_value = \
                self.symbols_dict[0]['p'][control_point_index][dimension_index]
            if type(dict_value) is not Symbol:
                # The variable is hard coded
                return dict_value
            else:
                # The variable is variable
                var_index = self.var_list[1].index(dict_value)
                return vector[var_index]

        control_points = []
        for control_point_index in range(self.bezier_degree+1):
            control_point = []
            for dimension_index in range(self.dim):
                control_point.append(var_lookup(control_point_index,
                                                dimension_index))

            control_points.append(Point(control_point))
        return BezierCurve(control_points, self.time_allocation)
