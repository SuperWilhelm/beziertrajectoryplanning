from geometry.point import Point
from geometry.bezier_curve_sequence import BezierCurveSequence
from geometry.bernstein_polynomial import bernstein_poly
from geometry.bezier_curve import BezierCurve
from sympy.core.symbol import Symbol
import cvxpy as cp
import numpy as np


class DeCasteljauOptimization:

    def __init__(self, problem):
        self.problem = problem

    def solve(self):
        '''
            This method when called will solve the problem using
            the DeCasteljau algorithm and Quadratic programming
        '''
        self.average_vel_heuristic = self.problem.average_vel_heuristic
        shortest_path = \
            self.problem.convex_free_space.get_shortest_path(
                self.problem.start_point,
                self.problem.end_point)

        self.polytopes = [x[0] for x in shortest_path][:-1]
        self.n_curves = len(self.polytopes)
        self.adjacent_polytopes = list(zip(self.polytopes,
                                           self.polytopes[1:]))

        self.intersection_polytopes = [
            self.problem.convex_free_space.intersection_graph[poly_0][poly_1]
            for (poly_0, poly_1) in self.adjacent_polytopes]

        # The knot points in this algorithm will infact be optimization
        # variables. In this case use the mean of the intersection polytopes
        # to estimate time heuristic
        self.knot_points = {}
        for i in range(self.n_curves):
            curve = {}
            if i == 0:
                curve['start'] = self.problem.start_point
            else:
                curve['start'] = self.intersection_polytopes[i-1].mean

            if i == self.n_curves - 1:
                curve['end'] = self.problem.end_point
            else:
                curve['end'] = self.intersection_polytopes[i].mean
            self.knot_points[self.polytopes[i]] = curve

        self.time_allocations = [Point.distance(self.knot_points[x]['start'],
                                                self.knot_points[x]['end']) /
                                 self.average_vel_heuristic
                                 for x in self.knot_points]

        print("The time allocations look like: {}".format(
            self.time_allocations))

        A, b, self.var_list_global, self.var_dict_global, c = \
            BezierCurveSequence.get_multi_curve_cost_function_matrix_form(
                1, self.problem.dim, self.problem.bezier_degree,
                start_point=self.problem.start_point,
                end_point=self.problem.end_point,
                var_post_fix='global')

        # Evaluate the functions A and b at the time allocarion
        A = np.array(A(sum(self.time_allocations)))
        b = np.array(b(sum(self.time_allocations)))
        c = np.array(c(sum(self.time_allocations)))

        _, _, var_list_local, var_dict_local, _ = \
            BezierCurveSequence.get_multi_curve_cost_function_matrix_form(
                self.n_curves, self.problem.dim, self.problem.bezier_degree,
                start_point=self.problem.start_point,
                end_point=self.problem.end_point,
                var_post_fix='local')

        F = BezierCurveSequence.get_inequality_constraints(
            self.n_curves, self.problem.dim, self.problem.bezier_degree,
            var_dict_local, self.polytopes,
            self.intersection_polytopes)

        variable_mapping = self.get_variable_mapping(self.time_allocations,
                                                     self.var_dict_global,
                                                     var_dict_local)

        global_constraints = []
        for x in F:
            try:
                global_constraints.append(x.subs(variable_mapping))
            except AttributeError:
                global_constraints.append(x)
                continue

        G, h = BezierCurveSequence.parse_linear_equation_to_matrix(
            global_constraints, self.var_list_global)

        G = np.array(G(0))
        h = np.array(h(0))
        h = h.reshape((h.shape[0],))

        n = len(self.var_list_global[1])
        x = cp.Variable(n)
        prob = cp.Problem(cp.Minimize(
                   (1/2)*cp.quad_form(x, A) + b@x + c),
                   [G@x <= h]
                   )
        value = prob.solve()

        return self.polytopes, self.parse_solution_to_bezier(
            x.value), value, prob.solver_stats.solve_time

    def get_variable_mapping(self, time_allocations, super_var_dict,
                             local_var_dict):
        '''
            Args:
                time_allocations: The time allocations for each of the
                    subcurves
                super_var_dict: Dictionary of the optimization variables
                local_var_dict: The local variable dictionay
            Returns:
                {sub_variable: supervariable_formula_mapping}
        '''
        def get_super_curve_control_points(super_var_dict):
            '''
                Args:
                    super_var_dict: Variable dictionary of the super curve
                Returns:
                    [Point] describing the control points of the super curve
            '''
            points_dict = super_var_dict[0]['p']

            points_list = []
            for i in points_dict:
                point_list = []
                for d in points_dict[i]:
                    point_list.append(points_dict[i][d])
                points_list.append(Point(point_list))
            return points_list

        def get_lhs_rhs_vars(variable_list, fraction):
            '''
                Args:
                    variable_list: List of the control points of the global
                        curve
                    fraction: The fraction ot the curve to go into lhs
                        The rest will go into rhs:
                Returns:
                    (lhs: List of expressions for each control point in the LHS
                     rhs: List of expressions for each control point in the RHS
                    )
            '''
            return ([sum([bernstein_poly(j, i, fraction)*variable_list[j]
                         for j in range(i+1)])
                     for i in range(self.problem.bezier_degree+1)],
                    [sum([bernstein_poly(j, i, fraction) *
                          variable_list[j+self.problem.bezier_degree-i]
                         for j in range(i+1)])
                     for i in range(self.problem.bezier_degree+1)][::-1]
                    )

        # The De Casteljau splitting will be in the following way:
        #                     Full Curve
        #                       /    \
        #                    Curve   /\
        #                      0    /  \
        #                        Curve /\
        #                          1  .  .
        #                            .    .
        # Create a list to store the control points each of the curves
        sub_curve_control_points = []
        # Create a list of the super curve control points
        rhs = get_super_curve_control_points(
            super_var_dict)
        time_in_rhs = sum(time_allocations)
        for i in range(self.n_curves):
            sub_curve_time = time_allocations[i]
            fraction = sub_curve_time/time_in_rhs
            lhs, rhs = get_lhs_rhs_vars(rhs, fraction)
            sub_curve_control_points.append(lhs)

        # now crearte a dict mapping the {local_var: high_level_formulae}
        d = {}
        for curve_index in range(self.n_curves):
            for cp_index in range(self.problem.bezier_degree):
                for dim_index in range(self.problem.dim):
                    var = local_var_dict[curve_index]['p'][cp_index][dim_index]
                    if type(var) is Symbol:
                        d[var] = sub_curve_control_points[curve_index][
                            cp_index].coords[dim_index]
        return d

    def parse_solution_to_bezier(self, solution_vector):
        '''
            Args:
                solution_vector
            Returns:
                [BezierCurve]
        '''
        def control_point_lookup(control_point_index,
                                 dimension_index):
            '''
                Args:
                    control_point_index: The index of the control point
                    dimension_index: The dimension of the index
                Returns:
                    real number to the value described in the argument's
                        of the solution curve
            '''
            cp_index = control_point_index
            dim_index = dimension_index
            value = self.var_dict_global[0]['p'][cp_index][dim_index]
            if type(value) is not Symbol:
                return value
            else:
                var_index = self.var_list_global[1].index(value)
                return solution_vector[var_index]

        curve_time = sum(self.time_allocations)
        control_points = []
        for control_point_index in range(self.problem.bezier_degree + 1):
            point = []
            for dim_index in range(self.problem.dim):
                point.append(control_point_lookup(control_point_index,
                                                  dim_index))
            control_points.append(Point(point))

        return BezierCurveSequence(
            [BezierCurve(control_points, curve_time)])
