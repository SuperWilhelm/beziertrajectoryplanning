# BezierTrajectoryPlanning
This repository contains an implementation of trajectory planning using
numerical optimization methods over the search space of Bezier Curves.
The methods currently implemented are:
*  Fixed time Naive implementation for each polytope
*  Fixed time Sophisticated implementation
*  De Casteljau Quadratic Programming
*  Non-Convex trust region search for optimal.
*  Bilelel optimization search method
  

These methods are all described inside the [final_manusctipt.pdf](https://gitlab.com/SuperWilhelm/beziertrajectoryplanning/-/blob/master/final_manuscript.pdf). Also inculded inside this document is details of how to run example problems. In particular, [example.py](https://gitlab.com/SuperWilhelm/beziertrajectoryplanning/-/blob/master/example.py) can be used as a starting point for future development. 